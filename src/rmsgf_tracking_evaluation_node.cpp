
/**
 * \file rmsgf_tracking_evaluation.cpp
 * \date September 2015
 * \author Jan Issac (jan.issac@gmail.com)
 */


#include <ctime>
#include <memory>
#include <mutex>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <Eigen/Dense>

#include <ros/ros.h>
#include <ros/package.h>
#include <std_msgs/Header.h>

#include <image_transport/image_transport.h>
#include <sensor_msgs/CameraInfo.h>
#include <stereo_msgs/DisparityImage.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/PointCloud2.h>

#include <boost/filesystem.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/regex.hpp>

#include <oni_vicon_common/global_calibration.hpp>
#include <oni_vicon_common/local_calibration.hpp>
#include <oni_vicon_common/calibration_reader.hpp>
#include <oni_vicon_common/type_conversion.hpp>

#include <oni_vicon_playback/exceptions.hpp>
#include <oni_vicon_playback/oni_vicon_player.hpp>

#include <fl/util/types.hpp>
#include <fl/util/meta.hpp>
#include <fl/util/profiling.hpp>

#include <dbot/utils/rigid_body_renderer.hpp>
#include <dbot/utils/helper_functions.hpp>

#include <state_filtering/utils/image_publisher.hpp>
#include <state_filtering/utils/ros_interface.hpp>
#include <state_filtering/utils/pcl_interface.hpp>

#include <dbot/states/free_floating_rigid_bodies_state.hpp>
#include <state_filtering/utils/ros_interface.hpp>
#include <state_filtering/utils/object_file_reader.hpp>

#include <rmsgf_tracking_ros_pkg/tracker/rmsgf_object_tracker.hpp>

using namespace rmsgf;
using namespace oni_vicon;
using namespace oni_vicon_playback;

std::string uirToPath(std::string uri)
{
    std::string re_str = "package://([\\w\\-]+)(/.+)";
    boost::cmatch matches;
    std::string package_path;
    std::string sub_path;

    try
    {
       boost::regex re(re_str);
       if (!boost::regex_match(uri.c_str(), matches, re))
       {
          std::cout << "Your URL is not formatted correctly!" << std::endl;
          throw "Your URL is not formatted correctly!";
       }

       package_path = std::string(matches[1].first, matches[1].second);
       sub_path = std::string(matches[2].first, matches[2].second);
    }
    catch (boost::regex_error& e)
    {
       std::cerr << "The regexp " << re_str << " is invalid!" << std::endl;
       throw(e);
    }

    return ros::package::getPath(package_path) + sub_path;
}


std::string recordName(const std::string& uri)
{
    std::string re_str = "/([\\w\\-]+/)+([\\w\\-]+)";
    boost::cmatch matches;

    try
    {
       boost::regex re(re_str);
       if (!boost::regex_match(uri.c_str(), matches, re))
       {
          std::cout << "Your URL is not formatted correctly!" << std::endl;
          throw "Your URL is not formatted correctly!";
       }

       return std::string(matches[2].first, matches[2].second);
    }
    catch (boost::regex_error& e)
    {
       std::cerr << "The regexp " << re_str << " is invalid!" << std::endl;
       throw(e);
    }
}

class ObjectTrackerNode
{
public:
    /* ------------------------------ */
    /* - The Filter                 - */
    /* ------------------------------ */
    typedef RmsgfObjectTracker<
        fl::RobustMultiSensorGaussianFilter,
        fl::UniformObservationModel,
        fl::SigmaPointQuadrature<fl::UnscentedTransform>,
        fl::PoseVelocityVector
    > Tracker;

    typedef typename Tracker::State State;

    struct Parameter
    {
        Parameter(Args args)
            : tracker_param(args)
        {
            args.get("gt_color_r", gt_color_r);
            args.get("gt_color_g", gt_color_g);
            args.get("gt_color_b", gt_color_b);

            args.get("belief_color_r", belief_color_r);
            args.get("belief_color_g", belief_color_g);
            args.get("belief_color_b", belief_color_b);

            args.get("downsampling", downsampling);

            height = 480 / downsampling;
            width = 640 / downsampling;
        }

        int downsampling;

        int gt_color_r;
        int gt_color_g;
        int gt_color_b;

        int belief_color_r;
        int belief_color_g;
        int belief_color_b;

        int height;
        int width;

        typename Tracker::Parameter tracker_param;

        void print()
        {
            PF(height);
            PF(width);
            PF(downsampling);

            tracker_param.print();
        }
    };

    typedef std::shared_ptr<Tracker> TrackerPtr;
    typedef std::shared_ptr<dbot::RigidBodyRenderer> RigidBodyRendererPtr;
public:
    ObjectTrackerNode(ros::NodeHandle& nh,
                      Eigen::Matrix3d init_camera_matrix,
                      std::string model_uri)
        : object_model_uri(model_uri),
          param(Args(nh)),
          last_measurement_time_(std::numeric_limits<fl::Real>::quiet_NaN())

    {
        param.print();

        /* ------------------------------ */
        /* - Setup camera               - */
        /* ------------------------------ */
        std::cout << "reading data from camera " << std::endl;
        camera_matrix = init_camera_matrix;
        camera_matrix(0, 0) /= param.downsampling; // fx
        camera_matrix(1, 1) /= param.downsampling; // fy
        camera_matrix(2, 2) = 1.0;
        camera_matrix(0, 2) /= param.downsampling;   // cx
        camera_matrix(1, 2) /=  param.downsampling;   // cy

        object_publisher =
            nh.advertise<visualization_msgs::Marker>("object_model", 0);
    }

    void initialize_tracker(const State& initial_pose)
    {
        tracker =std::shared_ptr<Tracker>(
            new Tracker(
                initial_pose,
                create_object_renderer(param, camera_matrix),
                param.tracker_param));
    }

    void track(const sensor_msgs::ImagePtr& ros_image)
    {
        std::lock_guard<std::mutex> lock(mutex);

        if(std::isnan(last_measurement_time_))
            last_measurement_time_ = ros_image->header.stamp.toSec();
        fl::Real delta_time =
                ros_image->header.stamp.toSec() - last_measurement_time_;
        last_measurement_time_ = ros_image->header.stamp.toSec();
        std::cout << "actual delta time " << delta_time << std::endl;

        image = ri::Ros2EigenVector<double>(*ros_image, param.downsampling);

//        INIT_PROFILING
        tracker->filter_once(image);
//        MEASURE("tracker->filter(image)");

//        PVT(tracker->belief().mean());
//        PV(tracker->belief().covariance());

        ri::PublishMarker(
            tracker->belief().mean().homogeneous().cast<float>(),
            ros_image->header,
            object_model_uri,
            object_publisher,
            1,
            param.belief_color_r/255.,
            param.belief_color_g/255.,
            param.belief_color_b/255., 1.0,
            "rmsgf_object_belief");
    }

    State current_belief_pose() const
    {
        return tracker->belief().mean();
    }

public: /* Factory Functions  */
    RigidBodyRendererPtr create_object_renderer(
        Parameter& params, Eigen::Matrix3d& camera_matrix)
    {
        std::cout << "Opening object file " << uirToPath(object_model_uri) << std::endl;
        std::vector<Eigen::Vector3d> object_vertices;
        std::vector<std::vector<int>> object_triangle_indices;
        ObjectFileReader file_reader;
        file_reader.set_filename(uirToPath(object_model_uri));
        file_reader.Read();
        object_vertices = *file_reader.get_vertices();
        object_triangle_indices = *file_reader.get_indices();

        RigidBodyRendererPtr object_renderer(
                new dbot::RigidBodyRenderer(
                    {object_vertices},
                    {object_triangle_indices},
                    camera_matrix,
                    param.height,
                    param.width
                )
        );

        return object_renderer;
    }

protected:
    std::string object_model_uri;
    Parameter param;
    Eigen::Matrix3d camera_matrix;

private:
    //Eigen::Matrix<Eigen::Vector3d, -1, -1> points;
    Eigen::VectorXd image;

    std::shared_ptr<Tracker> tracker;
    std::mutex mutex;

    fl::Real last_measurement_time_;

public:
    ros::Publisher object_publisher;
};



int main (int argc, char **argv)
{
    /* ------------------------------ */
    /* - Setup ros                  - */
    /* ------------------------------ */
    ros::init(argc, argv, "rmsgf_object_tracker");
    ros::NodeHandle nh("~");

    /* ------------------------------ */
    /* - Create oni player          - */
    /* ------------------------------ */
    OniPlayer::Ptr oni_player = boost::make_shared<OniPlayer>();
    ViconPlayer::Ptr vicon_player = boost::make_shared<ViconPlayer>();
    OniViconPlayer::Ptr playback = boost::make_shared<OniViconPlayer>(oni_player, vicon_player);

    /* ------------------------------ */
    /* - Get data set parameter     - */
    /* ------------------------------ */
    std::string depth_frame_id = "/XTION_IR";
    std::string record_path = "";
    std::string output_path = "";
    std::string result_file = "";
    std::string plot_file = "";
    std::ofstream ofs_;
    nh.param("record_path", record_path, record_path);
    nh.param("output_path", output_path, output_path);
    result_file = output_path + "/" + recordName(record_path) + "_results.txt";


    /* ------------------------------ */
    /* - Open data set              - */
    /* ------------------------------ */
    try
    {
        ROS_INFO("Opening data set %s ...", record_path.c_str());
        playback->open(record_path);
        ROS_INFO("Data set record loaded.");
    }
    catch(std::exception& e)
    {
        ROS_WARN("Loading record failed: %s ", e.what());
        return 1;
    }


    /* ------------------------------ */
    /* - Set playback position at 0 - */
    /* ------------------------------ */
    ros::Time startup_time = ros::Time::now();

    playback->play(0);
    ROS_INFO("Playing data set.");

    /* ------------------------------ */
    /* - Get camera matrix from data- */
    /* - set                        - */
    /* ------------------------------ */
    sensor_msgs::CameraInfoPtr camera_info =
            oni_vicon::toCameraInfo(playback->transformer().cameraIntrinsics());

    Eigen::Matrix3d camera_matrix;
    for(unsigned int col = 0; col < 3; col++)
    {
        for(unsigned int row = 0; row < 3; row++)
        {
            camera_matrix(row,col) = camera_info->K[col+row*3];
        }
    }

    /* ------------------------------ */
    /* - Initialize trakcer         - */
    /* ------------------------------ */
    ObjectTrackerNode tracker_node(
        nh,
        camera_matrix,
        playback->transformer().localCalibration().objectDisplay());

    /* ------------------------------ */
    /* - Initialize visualization   - */
    /* ------------------------------ */
    ros::Publisher cloud_publisher =
        nh.advertise<sensor_msgs::PointCloud2> ("/playback/depth/points", 0);

    /* ------------------------------ */
    /* - playback data and evaluate - */
    /* ------------------------------ */
    bool init = false;

    while (ros::ok() && playback->isPlaying())
    {
        uint32_t frame_id = playback->nextFrame();

        // get depth sensor frame and corresponding vicon frame
        sensor_msgs::ImagePtr depth_msg =
            playback->oniPlayer()->currentDepthImageMsg();
        ViconPlayer::PoseRecord vicon_pose_record_dts =
            playback->currentViconPose();
        ViconPlayer::PoseRecord vicon_pose_record =
            playback->viconPlayer()->pose(frame_id);

        if (vicon_pose_record.stamp.isZero())
        {
            break;
        }

        // this is the shifted time
        ros::Time stamp;
        stamp.fromSec(startup_time.toSec() + vicon_pose_record.stamp.toSec());

        // set meta data and publish
        depth_msg->header.stamp = stamp;
        depth_msg->header.frame_id = depth_frame_id;


        if (!init)
        {
            ObjectTrackerNode::State init_pose;

            init_pose.position()(0) = vicon_pose_record_dts.pose.getOrigin().getX();
            init_pose.position()(1) = vicon_pose_record_dts.pose.getOrigin().getY();
            init_pose.position()(2) = vicon_pose_record_dts.pose.getOrigin().getZ();
            auto q  = init_pose.orientation().quaternion();
            q.w() = vicon_pose_record_dts.pose.getRotation().getW();
            q.x() = vicon_pose_record_dts.pose.getRotation().getX();
            q.y() = vicon_pose_record_dts.pose.getRotation().getY();
            q.z() = vicon_pose_record_dts.pose.getRotation().getZ();
            init_pose.orientation().quaternion(q);

            tracker_node.initialize_tracker(init_pose);

            init = true;
            continue;
        }



        sensor_msgs::PointCloud2Ptr points_msg =
            boost::make_shared<sensor_msgs::PointCloud2>();

        oni_vicon::toMsgPointCloud(depth_msg,
                                   playback->transformer().cameraIntrinsics(),
                                   points_msg);
        cloud_publisher.publish(points_msg);

        tracker_node.track(depth_msg);
        auto rmsgf_pose = tracker_node.current_belief_pose();


        std::cout << "vicon pose "
                  << vicon_pose_record_dts.pose.getOrigin().getX() << " "
                  << vicon_pose_record_dts.pose.getOrigin().getY() << " "
                  << vicon_pose_record_dts.pose.getOrigin().getZ() << " "
                  << vicon_pose_record_dts.pose.getRotation().getW() << " "
                  << vicon_pose_record_dts.pose.getRotation().getX() << " "
                  << vicon_pose_record_dts.pose.getRotation().getY() << " "
                  << vicon_pose_record_dts.pose.getRotation().getZ() << " "
                  << std::endl;

        std::cout << "rmsgf  pose "
                  << rmsgf_pose.position()(0) << " "
                  << rmsgf_pose.position()(1) << " "
                  << rmsgf_pose.position()(2) << " "
                  << rmsgf_pose.orientation().quaternion().w()  << " "
                  << rmsgf_pose.orientation().quaternion().x()  << " "
                  << rmsgf_pose.orientation().quaternion().y()  << " "
                  << rmsgf_pose.orientation().quaternion().z()  << " "
                  << std::endl;

        fl::PoseVector gt_pose;

        gt_pose.position()(0) = vicon_pose_record_dts.pose.getOrigin().getX();
        gt_pose.position()(1) = vicon_pose_record_dts.pose.getOrigin().getY();
        gt_pose.position()(2) = vicon_pose_record_dts.pose.getOrigin().getZ();
        auto q  = gt_pose.orientation().quaternion();
        q.w() = vicon_pose_record_dts.pose.getRotation().getW();
        q.x() = vicon_pose_record_dts.pose.getRotation().getX();
        q.y() = vicon_pose_record_dts.pose.getRotation().getY();
        q.z() = vicon_pose_record_dts.pose.getRotation().getZ();
        gt_pose.orientation().quaternion(q);

        ri::PublishMarker(
            gt_pose.homogeneous().cast<float>(),
            depth_msg->header,
            playback->transformer().localCalibration().objectDisplay(),
            tracker_node.object_publisher, 1,
            97/255., 117/255., 123/255., 1.0,
            "vicon_object");
    }

    ROS_INFO("Playback ended.");

    playback->stop();
    playback->close();
    ofs_.close();

    return 0;
}
